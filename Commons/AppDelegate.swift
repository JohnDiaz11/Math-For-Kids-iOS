//
//  AppDelegate.swift
//  AnimalesApp
//
//  Created by John Diaz on 22/09/22.
//

import UIKit
import FirebaseCore
import GoogleMobileAds


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Use the Firebase library to configure APIs.
        FirebaseApp.configure()
        //GADMobileAds.sharedInstance().start(completionHandler: nil)
        return true
    }

    
}

